package com.dr_s.autogui.ui.notifications;

import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.dr_s.autogui.AppLogger;
import com.dr_s.autogui.R;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class NotificationsFragment extends Fragment {

    private NotificationsViewModel notificationsViewModel;

    //open new file
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        notificationsViewModel =
                ViewModelProviders.of(this).get(NotificationsViewModel.class);
        View root = inflater.inflate(R.layout.fragment_notifications, container, false);
        TextView textView = root.findViewById(R.id.text_notifications);
        textView.setMovementMethod(new ScrollingMovementMethod());
        getLog(textView);
        return root;
    }

    private void getLog(TextView tv){
        tv.setText(AppLogger.getLog());
        //try {
        //    Process process = Runtime.getRuntime().exec("logcat -d");
        //    BufferedReader bufferedReader = new BufferedReader(
        //            new InputStreamReader(process.getInputStream()));
//
        //    StringBuilder log=new StringBuilder();
        //    String line = "";
        //    while ((line = bufferedReader.readLine()) != null) {
        //        log.append(line);
        //    }
        //    tv.setText(log.toString());
        //}
        //catch (IOException e) {}
    }
}