package com.dr_s.autogui.ui.dashboard;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class DashboardViewModel extends ViewModel {

    private MutableLiveData<String> mText;

    public DashboardViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("fix or verify the coordinates in this file with android pointer location");
    }

    public LiveData<String> getText() {
        return mText;
    }
}