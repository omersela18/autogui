package com.dr_s.autogui;

import android.app.Service;

import android.content.Intent;

import android.content.IntentFilter;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.Settings;
import android.util.Log;
import android.widget.Toast;

import com.dr_s.androidautogui.Interpreter;
import com.dr_s.androidautogui.Screen;

import java.util.logging.Logger;


public class InterpretFile extends Service {
    public static final String FILE_DATA = "data";
    private double last_acc = 0;
    private double curr_acc = 0;
    private double acc = 0;
    private Interpreter interpreter = null;
    private Runnable r = new Runnable() {
        @Override
        public void run() {
            interpreter.run_code();
        }
    };

    public InterpretFile() {

    }


    private final SensorEventListener listener = new SensorEventListener() {
        @Override
        public void onSensorChanged(SensorEvent event) {

            double x = event.values[0];
            double y = event.values[1];
            double z = event.values[2];

            curr_acc = Math.sqrt(x*x + y*y + z*z);
            acc = acc*0.9f + (curr_acc - last_acc);
            last_acc = acc;
            //needs to test outside emulator
            if(acc > 13){
                stop();
                Log.i("fun", "shake detected");
                //interpreter.terminate();
                //Toast.makeText(getApplicationContext(), "shake detected", Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        public void onAccuracyChanged(Sensor sensor, int accuracy) {

        }
    };


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Bundle extra = intent.getExtras();
        SensorManager sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        sensorManager.registerListener(listener, sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), SensorManager.SENSOR_DELAY_NORMAL);
        try {
            interpreter = new Interpreter(this, extra.getString(FILE_DATA));
        } catch (Screen.ScreenException e) {
            Log.println(Log.INFO, "fun", e.getMessage());
            Screen.ScreenException.requestPermissions(this);
        }
        Thread thread = null;
        if (interpreter != null){
            thread = new Thread(r);
            thread.start();
        }

        return super.onStartCommand(intent, flags, startId);
    }

    private void stop(){
        this.registerReceiver(interpreter, new IntentFilter("com.dr_s.androidautogui.STOP"));
        this.sendBroadcast(new Intent("com.dr_s.androidautogui.STOP"));
    }


    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }
}
