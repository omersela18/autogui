package com.dr_s.autogui;

import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;

public class Editor extends AppCompatActivity {

    static public String FILE_EXTRA = "file";
    private String file_name = "default";
    private String data = "test"; //update with text box
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editor);
        EditText text = findViewById(R.id.file_data);
        text.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                data = s.toString();
                saveFile();
            }
        });
        getFileName();
        openFile(text);
        FloatingActionButton run = findViewById(R.id.run_button);
        run.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                interpret();
            }
        });

    }

    protected void interpret(){
        Intent interpreter = new Intent(this, InterpretFile.class);
        interpreter.putExtra(InterpretFile.FILE_DATA, data);
        startService(interpreter);
    }

    protected void getFileName(){
        Bundle extra =  getIntent().getExtras();
        file_name = extra.getString(FILE_EXTRA);
    }


    protected void saveFile(){
        FileOutputStream outputStream;

        try {
            outputStream = openFileOutput(file_name, MODE_PRIVATE);
            outputStream.write(data.getBytes());
            outputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    protected void openFile(EditText text){
        FileInputStream inputStream;

        try{
            inputStream = openFileInput(file_name);

            BufferedReader r = new BufferedReader(new InputStreamReader(inputStream));
            StringBuilder total = new StringBuilder();
            for (String line; (line = r.readLine()) != null; ) {
                total.append(line).append('\n');
            }
            data = total.toString();
            text.setText(data);
            inputStream.close();
        } catch(Exception e){
            e.printStackTrace();
        }
    }
}
