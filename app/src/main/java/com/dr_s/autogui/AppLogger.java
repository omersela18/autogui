package com.dr_s.autogui;

import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class AppLogger {
    public static StringBuilder getLog() {
        StringBuilder builder = new StringBuilder();

        try {
            String[] command = new String[] { "logcat", "-d", "-v", "threadtime" };

            Process process = Runtime.getRuntime().exec(command);

            BufferedReader bufferedReader = new BufferedReader(
                    new InputStreamReader(process.getInputStream()));

            String line;
            while ((line = bufferedReader.readLine()) != null) {
                if (line.contains("fun")) {
                    line = line.split("fun", 2)[1];
                    builder.append(line);
                    builder.append("\n");
                }
            }
        } catch (IOException ex) {
            Log.e("fun", "getLog failed", ex);
        }
        return builder;
    }
}
