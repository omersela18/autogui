package com.dr_s.androidautogui;

import android.accessibilityservice.AccessibilityService;
import android.accessibilityservice.AccessibilityServiceInfo;
import android.accessibilityservice.GestureDescription;
import android.content.Context;
import android.content.Intent;
import android.graphics.Path;
import android.provider.Settings;
import android.util.Log;
import android.view.KeyCharacterMap;
import android.view.KeyEvent;
import android.view.ViewManager;
import android.view.WindowManager;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import android.view.inputmethod.InputMethodManager;


//singleton
public class Screen extends AccessibilityService {

    private static Screen instance;

    public static class ScreenException extends Exception{
        public ScreenException(){
            super("service is not running, use static function requestPermissions to run");
        }
        public static void requestPermissions(Context c){
            Intent openSettings = new Intent(Settings.ACTION_ACCESSIBILITY_SETTINGS);
            openSettings.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_NO_HISTORY);
            c.startActivity(openSettings);
        }
    }

    public static synchronized Screen getInstance() throws ScreenException{
        if(instance == null){
            throw new ScreenException();
        }
        return instance;
    }


    private GestureDescription GenerateClick(int x, int y){
        GestureDescription.Builder builder = new GestureDescription.Builder();
        Path path = new Path();
        path.moveTo((float)x, (float)y);
        builder.addStroke(new GestureDescription.StrokeDescription(path, 0, 1));
        return builder.build();
    }
    private GestureDescription GenerateSlide(int x1, int y1, int x2, int y2){
        GestureDescription.Builder builder = new GestureDescription.Builder();
        Path path = new Path();
        path.moveTo((float)x1, (float)y1);
        path.lineTo((float)x2, (float)y2);
        builder.addStroke(new GestureDescription.StrokeDescription(path, 0, 10));
        return builder.build();
    }


    public Screen(){
        instance = this;
        Log.println(Log.INFO, "fun", "created screen");
    }

    public void Click(int x, int y){
        boolean result = dispatchGesture(GenerateClick(x, y), null, null);
        Log.println(Log.INFO, "fun", "click " + x + " " + y + " result is " + result);
    }

    public void Slide(int x1, int y1, int x2, int y2){
        boolean result = dispatchGesture(GenerateSlide(x1, y1, x2, y2), null, null);
        Log.println(Log.INFO, "fun", "slide result is " + result);
    }

    public void Type(String s){
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
        char[] szRes = s.toCharArray(); // Convert String to Char array

        KeyCharacterMap CharMap;
        CharMap = KeyCharacterMap.load(KeyCharacterMap.VIRTUAL_KEYBOARD);
        KeyEvent[] events = CharMap.getEvents(szRes);
        //WindowManager wm = (WindowManager) getSystemService(WINDOW_SERVICE);
        AccessibilityNodeInfo nodeInfo = getRootInActiveWindow();


        for(int i=0; i<events.length; i++) {
            inputMethodManager.dispatchKeyEventFromInputMethod(null, events[i]);
        }
        Log.println(Log.INFO, "fun", "type " + s);
    }

    public void HomeButton(){
        performGlobalAction(GLOBAL_ACTION_HOME);
    }

    @Override
    protected void onServiceConnected() {
        Log.println(Log.INFO, "fun", "connected");
        AccessibilityServiceInfo info = new AccessibilityServiceInfo();
        info.eventTypes = AccessibilityEvent.TYPE_VIEW_CLICKED + AccessibilityEvent.TYPE_VIEW_CONTEXT_CLICKED;
        info.notificationTimeout = 100;
        info.feedbackType = AccessibilityEvent.TYPES_ALL_MASK;
        setServiceInfo(info);
        //super.onServiceConnected();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.i("fun", "start");
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onAccessibilityEvent(AccessibilityEvent event) {
        Log.i("fun", "event happened");

        if(event.getEventType() == AccessibilityEvent.TYPE_VIEW_CLICKED){
            try {
                Log.i("fun", "clicked " + event.getSource().toString());
            }
            catch(Exception e) {
                Log.e("fun", "error while handling event");
            }
        }
    }

    @Override
    protected boolean onKeyEvent(KeyEvent event) {
        Log.i("fun", event.toString());
        return super.onKeyEvent(event);
    }

    @Override
    public void onInterrupt() {
        Log.println(Log.INFO, "fun", "int");
    }

    @Override
    public boolean onUnbind(Intent intent) {
        Log.println(Log.INFO, "fun", "unbind");
        return super.onUnbind(intent);
    }
}
