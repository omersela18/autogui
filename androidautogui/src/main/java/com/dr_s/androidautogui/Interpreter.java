package com.dr_s.androidautogui;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.util.Log;

import androidx.core.app.NotificationCompat;

import java.security.InvalidParameterException;
import java.util.HashMap;
import java.util.Stack;

public class Interpreter extends BroadcastReceiver{
    private String data;
    Context context;
    private Screen screen;

    private int IP = 0; //next line to run, starts at 0
    private Stack<Integer> IPS = new Stack<>(); // stack of last IPS, stack frame
    private int CX = 0; // loop register
    private HashMap<String, String> VD = new HashMap<>(); //variables dictionary, the key will be the variable name and the value is the string of the variable
    private HashMap<String, String> FD = new HashMap<>(); //functions dictionary, the key will be the function name and the value is the first line of the function
    private String[] builtin_functions = {"click", "type", "add", "sub", "inc", "dec", "mov", "notify", "wait", "home", "swipe", "call", "jmp", "ret", "launch"};

    public Interpreter(Context c, String s) throws Screen.ScreenException{
        context = c;
        data = s;
        screen = Screen.getInstance();
    }

    private String[] split_lines(String str){
        return str.split("\n");
    }

    private String[] split_args(String str){
        return str.split(" ");
    }


    public void run_code(){
        // notify_close();
        String[] lines = split_lines(data);
        IP = 0;
        while(IP < lines.length){
            try {
                IP = execute(lines[IP]);
            }catch (InvalidParameterException e){
                e.printStackTrace();
                Log.println(Log.INFO, "fun", "invalid parameters in line " + IP);
                break;
            }
        }
        IP = 0;
    }

    private int execute(String line){
        String[] cmd = split_args(line);
        boolean built = false;
        for (String element : builtin_functions) {
            if (element.equals(cmd[0])) {
                built = true;
                break;
            }
        }
        if(built){
            builtins(cmd);
        }
        else{
            Log.println(Log.INFO, "fun", "cant run not builtin at the moment");
        }
        //returns next IP
        return IP + 1;
    }

    private void notify(String message){
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel("notify", "command", NotificationManager.IMPORTANCE_DEFAULT);
            NotificationManager mNotificationManager =
                    (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

            mNotificationManager.createNotificationChannel(channel);

            NotificationCompat.Builder builder = new NotificationCompat.Builder(context, "notify")
                    .setSmallIcon(androidx.core.R.drawable.notification_bg)
                    .setContentTitle("interpreting")
                    .setContentText(message)
                    .setPriority(NotificationCompat.PRIORITY_DEFAULT);

            mNotificationManager.notify(2, builder.build());
            //should show option to close the interpreter
        }
    }

    //creates new string from strings array with string s in the middle, starts from offset
    private String join(String[] strings, String s, int offset){
        String message = strings[offset];
        for(int i=offset+1; i < strings.length;i++){
            message += " " + strings[i];
        }
        return message;
    }

    //wanted hash map of functions... will have to do a switch because java
    private void builtins(String[] cmd) {
        switch (cmd[0]){
            case "click":
                if(cmd.length < 3)
                    throw new InvalidParameterException();
                screen.Click(Integer.parseInt(cmd[1]), Integer.parseInt(cmd[2]));
                break;
            case "type":
                if(cmd.length < 2)
                    throw new InvalidParameterException();
                screen.Type(join(cmd, " ", 1));
                break;
            case "notify":
                if(cmd.length < 2)
                    throw new InvalidParameterException();
                String message = join(cmd, " ", 1);
                notify(message);
                Log.println(Log.INFO, "fun", "notified " + message);
                break;
            case "wait":
                if(cmd.length < 2)
                    throw new InvalidParameterException();
                try {
                    Log.println(Log.INFO, "fun", "waiting " + cmd[1] + " milliseconds");
                    Thread.sleep(Integer.parseInt(cmd[1]));
                }catch (InterruptedException i){
                    i.printStackTrace();
                }

                break;
            case "home":
                screen.HomeButton();
                break;
            case "swipe":
                if(cmd.length < 5)
                    throw new InvalidParameterException();
                screen.Slide(Integer.parseInt(cmd[1]), Integer.parseInt(cmd[2]), Integer.parseInt(cmd[3]), Integer.parseInt(cmd[4]));
                break;
            case "launch":
                if(cmd.length < 2)
                    throw new InvalidParameterException();
                Intent launchIntent = context.getPackageManager().getLaunchIntentForPackage(cmd[1]);

                if (launchIntent != null) {
                    Log.println(Log.INFO, "fun", "launching " + cmd[1]);
                    context.startActivity(launchIntent);//null pointer check in case package name was not found
                }
                else{
                    Log.println(Log.INFO, "fun", cmd[1] + " not found");
                }
                break;
            case "jmp":
                if(cmd.length < 2)
                    throw new InvalidParameterException();
                IP = Integer.parseInt(cmd[1]) - 2;
                Log.i("fun", "jumped to " + IP);
                break;
            default:
                Log.println(Log.INFO, "fun", cmd[0] + " is not implemented at the moment");

        }
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.i("fun", "terminate at " + IP);
        this.IP = Integer.MAX_VALUE - 1; //trying to avoid possible integer overflow
    }
}
